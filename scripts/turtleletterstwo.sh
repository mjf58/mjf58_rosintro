#!/usr/bin/bash
rosservice call /kill "turtle1"
rosservice call /spawn 2 2 0.0 "morgan"
rosservice call /spawn 7 2.5 0.0 "haylee"
rosservice call /morgan/set_pen 100 230 0 2 0
rosservice call /haylee/set_pen 60 157 220 2 0
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.5]'
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 3.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.88]'
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 3.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.5]'
rostopic pub -1 /morgan/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /haylee/cmd_vel geometry_msgs/Twist -- '[0.0, 5.0, 0.0]' '[0.0, 0.0, -6.3]'
